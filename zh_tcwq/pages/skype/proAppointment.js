var app = getApp();
Page({
    data: {
        user_id: "",
    },
    onLoad: function (temp) {
        console.log(temp.id);
        var currentData = this;
        this.user_id = temp.id;
        currentData.setData({
            allAppointments: true,
            finishedAppointments: false,
            awaitingAppointments: false,
            hiddenAll: false,
            hiddenFinished: true,
            hiddenAwaiting: true,
        });
        app.util.request({
            url: "entry/wxapp/getproappointment",
            cachetime: "0",
            data: {
                user_id: this.user_id,
            },
            success: function (result) {
                console.log(result);
                currentData.setData({
                    appointments: result.data
                })
            }
        })
    },
    selectedAll: function (temp) {
        var currentData = this;
        app.util.request({
            url: "entry/wxapp/getproappointment",
            cachetime: "0",
            data: {
                user_id: this.user_id,
            },
            success: function (result) {
                console.log(result);
                currentData.setData({
                    appointments: result.data
                })
            }
        })
        currentData.setData({
            allAppointments: true,
            finishedAppointments: false,
            awaitingAppointments: false,
            hiddenAll: false,
            hiddenFinished: true,
            hiddenAwaiting: true,
        });
    },
    selectedFinished: function (temp) {
        var currentData = this;
        app.util.request({
            url: "entry/wxapp/getprofinishedappointment",
            cachetime: "0",
            data: {
                user_id: this.user_id,
            },
            success: function (result) {
                console.log(result);
                currentData.setData({
                    appointments: result.data
                })
            }
        })
        currentData.setData({
            allAppointments: false,
            finishedAppointments: true,
            awaitingAppointments: false,
            hiddenAll: true,
            hiddenFinished: false,
            hiddenAwaiting: true,
        });
    },
    selectedAwaiting: function (temp) {
        wx.showLoading({
            mask:true
        });
        var currentData = this;
        app.util.request({
            url: "entry/wxapp/getproawaitingappointment",
            cachetime: "0",
            data: {
                user_id: this.user_id,
            },
            success: function (result) {
                console.log(result);
                currentData.setData({
                    appointments: result.data
                })
                wx.hideLoading({});
            }
        })
        currentData.setData({
            allAppointments: false,
            finishedAppointments: false,
            awaitingAppointments: true,
            hiddenAll: true,
            hiddenFinished: true,
            hiddenAwaiting: false,
        });
        
    },
    approval: function (temp) {
        var currentData = this;
        var skype_id = temp.currentTarget.dataset.id;
        console.log('Weboost-JL: skype_id', skype_id);
        wx.showModal({
            title: "确认预约",
            content: "确认通过该预约申请吗？",
            success: function (res) {
                if (res.confirm) {
                    app.util.request({
                        url: "entry/wxapp/approvalappointment",
                        cachetime: "0",
                        data: {
                            id: skype_id
                        },
                        success: function (tempResult) {
                            console.log('Weboost-JL: tempResult', tempResult);
                            currentData.selectedAwaiting();
                        }
                    })
                } else if (res.cancel) {
                    console.log('Weboost-JL: cancel', res.cancel);

                }
            }
        })

    }
    

})