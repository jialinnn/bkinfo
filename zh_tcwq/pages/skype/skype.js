var app = getApp();

Page({
    data: {
        date: "",
        dateTomorrow: "",
        chooseDate: "",
        flag: false,
        time: ""
    },
    onLoad: function (temp) {
        console.log('Weboost-JL: temp', temp);
        var currentData = this;
        console.log('Weboost-JL: currentData', currentData);
        var user_id = wx.getStorageSync("users").id;
        app.util.request({
            url: "entry/wxapp/getcredit",
            cachetime: "0",
            data: {
                user_id: user_id,
            },
            success: function (resultNumber) {
                if (resultNumber.data.money < 50) {
                    wx.showModal({
                        title: "提示",
                        content: "积分不足，预约需要50积分",
                        showCancel: !0,
                        cancelText: "取消",
                        confirmText: "确定",
                        success: function (t) {
                            wx.reLaunch({
                                url: "../logs/logs",
                            })
                        },
                        fail: function (t) { },
                        complete: function (t) { }
                    })
                }
                console.log(resultNumber.data.money);

            }
        });
        var todayDate = new Date();
        var tomorrowDate = new Date(todayDate.getTime() + (24 * 60 * 60 * 1000));
        var year = todayDate.getFullYear();
        var month = todayDate.getMonth() + 1;
        var day = todayDate.getDate();
        var hour = todayDate.getHours();
        var mins = todayDate.getMinutes();
        var yearTomorrow = tomorrowDate.getFullYear();
        var monthTomorrow = tomorrowDate.getMonth() + 1;
        var dayTomorrow = tomorrowDate.getDate();
        if (mins < 10) {
            mins = "0" + mins;
        }
        console.log(year + "-" + month + "-" + day + "  " + hour + ":" + mins + "  " + yearTomorrow + "-" + monthTomorrow + "-" + dayTomorrow);
        this.date = year + "-" + month + "-" + day;
        this.dateTomorrow = yearTomorrow + "-" + monthTomorrow + "-" + dayTomorrow;
        this.chooseDate = this.dateTomorrow;
        this.time = hour + ":" + mins;
        console.log(this.date + "  " + this.dateTomorrow);
        currentData.setData({
            date: this.date,
            dateTomorrow: this.dateTomorrow,
            time: hour + ":" + mins,
            chooseDate: this.dateTomorrow,
        })
    },
    bindDateChange: function (e) {
        console.log('picker发送选择改变，携带值为', e.detail.value)
        this.chooseDate = e.detail.value;
        this.setData({
            dateTomorrow: this.chooseDate,
        })
    },
    bindTimeChange: function (e) {
        console.log('picker发送选择改变，携带值为', e.detail.value)
        this.setData({
            time: e.detail.value
        })
    },
    // chooseDate: function (temp) {
    //     console.log(temp);
    //     var currentData = this;
    //     console.log(currentData);
    //     //console.log(imcome);
    //     wx.navigateTo({
    //         url: "../calendar/calendar",
    //     })
    // },
    formSubmit: function (temp) {
        console.log(this.dateTomorrow);
        var currentData = this;
        var content = "";
        var currentPro = wx.getStorageSync("currentPro");
        var tomorrowDate = this.dateTomorrow;
        var tomorrowTime = this.time;
        console.log(currentPro);
        console.log(currentData);
        console.log(temp);
        var pro_id = currentPro.id;
        var pro_name = currentPro.store_name;
        var user_id = wx.getStorageSync("users").id;
        var name = temp.detail.value.name;
        var tel = temp.detail.value.tel;
        var email = temp.detail.value.email;
        var date = temp.detail.value.date;
        var time = temp.detail.value.time;
        var question = temp.detail.value.question;
        if (name == "" ? content = "姓名不能为空" : tel == "" ? content = "电话不能为空" : email == "" ? content = "邮箱地址不能为空" : question == "" ? content = "问题不能为空" : content = "") {
            wx.showModal({
                title: "提示",
                content: content,
                showCancel: !0,
                cancelText: "取消",
                confirmText: "确定",
                success: function (t) { },
                fail: function (t) { },
                complete: function (t) { }
            })
        } else {
            var phoneA = /^((04|03)\d{8})$/;//australian phone;
            var phoneC = /^1[3|4|5|7|8][0-9]{9}$/;//Chinese phone;
            var emailReg = /^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
            if (tel.length != 11 && tel.length != 10) {
                wx.showModal({
                    title: "提示",
                    content: "手机号码长度不对",
                    showCancel: !0,
                    cancelText: "取消",
                    confirmText: "确定",
                })
            } else if (!phoneA.test(tel) && !phoneC.test(tel)) {
                wx.showModal({
                    title: "提示",
                    content: "手机号码格式错误",
                    showCancel: !0,
                    cancelText: "取消",
                    confirmText: "确定",
                })
            } else if (!emailReg.test(email)) {
                wx.showModal({
                    title: "提示",
                    content: "请输入正确的email",
                    showCancel: !0,
                    cancelText: "取消",
                    confirmText: "确定",
                })
            } else {
                var tomorrowDateIs = this.dateTomorrow;
                console.log(this.dateTomorrow + " " + this.chooseDate);
                var dateFlag = this.dateTomorrow.split("-");
                var dateChoose = this.chooseDate.split("-");
                console.log(currentData);
                console.log(temp);
                var start = dateFlag[0] + "/" + dateFlag[1] + "/" + dateFlag[2];
                var end = dateChoose[0] + "/" + dateChoose[1] + "/" + dateChoose[2];
                console.log(start + " " + end);
                var startDateTemp = new Date(start);
                var endDateTemp = new Date(end);
                if (startDateTemp > endDateTemp) {
                    console.log(startDateTemp + ">" + endDateTemp);
                    this.flag = false;
                    this.chooseDate = this.dateTomorrow;
                } else if (startDateTemp < endDateTemp) {
                    console.log(startDateTemp + "<" + endDateTemp);
                    this.flag = true;
                } else {
                    console.log(startDateTemp + "=" + endDateTemp);
                    this.flag = true;
                }
                console.log(this.flag);
                console.log(this.chooseDate);
                console.log(this.time);
                if (this.flag == true) {
                    var serialDate = new Date();
                    var serialYear = serialDate.getFullYear();
                    var serialMonth = serialDate.getMonth() + 1;
                    if (serialMonth < 10) {
                        serialMonth = 0 + "" + serialMonth;
                    }
                    var serialDay = serialDate.getDate();
                    if (serialDay < 10) {
                        serialDay = 0 + "" + serialDay;
                    }
                    var serialHour = serialDate.getHours();
                    if (serialHour < 10) {
                        serialHour = 0 + "" + serialHour;
                    }
                    var serialMins = serialDate.getMinutes();
                    if (serialMins < 10) {
                        serialMins = 0 + "" + serialMins;
                    }
                    var serialNum = serialYear + "" + serialMonth + "" + serialDay + "" + serialHour + "" + serialMins;
                    console.log(serialNum);
                    app.util.request({
                        url: "entry/wxapp/addreservation",
                        cachetime: "0",
                        data: {
                            pro_id: pro_id,
                            pro_name: pro_name,
                            user_id: user_id,
                            user_name: name,
                            user_tel: tel,
                            user_email: email,
                            book_date: date,
                            book_time: time,
                            question: question,
                            serial_num: serialNum,
                        },
                        success: function (result) {

                            console.log(result);
                            //插入成功返回result.data进行判断是否预约成功，下面目前显示成功结果；
                            wx.showModal({
                                title: "提示",
                                content: "预约成功",
                                showCancel: !0,
                                cancelText: "取消",
                                confirmText: "确定",
                                success: function (t) {
                                    var user_id = wx.getStorageSync("users").id;
                                    app.util.request({
                                        url: "entry/wxapp/creditdeduct",
                                        cachetime: "0",
                                        data: {
                                            user_id: user_id,
                                            bounty: 50,
                                        },
                                        success: function (temp) {
                                        console.log(temp);
                                        }
                                    });
                                    wx.reLaunch({
                                        url: "../type/type",
                                    })
                                },
                                fail: function (t) { },
                                complete: function (t) { }
                            })
                        }
                    })
                }
                else {
                    wx.showModal({
                        title: "提示",
                        content: "抱歉，您必须提前至少一天预约",
                        showCancel: !0,
                        cancelText: "取消",
                        confirmText: "确定",
                        success: function (t) {
                            console.log(this.dateTomorrow);
                            console.log(tomorrowDate);
                            currentData.setData({
                                dateTomorrow: tomorrowDate,
                                time: tomorrowTime,
                            })
                        },
                        fail: function (t) { },
                        complete: function (t) { }
                    });
                }
            }
        }
    }
});