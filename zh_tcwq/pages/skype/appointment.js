var app = getApp();
Page({
    data:{
        user_id:"",
    },
    onLoad:function(temp){
        console.log(temp.id);
        var currentData = this;
        this.user_id = temp.id;
        currentData.setData({
            allAppointments: true,
            finishedAppointments: false,
            awaitingAppointments: false,
            hiddenAll: false,
            hiddenFinished: true,
            hiddenAwaiting: true,
        });
        app.util.request({
            url:"entry/wxapp/getmyappointment",
            cachetime: "0",
            data:{
                user_id: this.user_id,
            },
            success: function(result){
                    console.log(result);
                    currentData.setData({
                        appointments: result.data
                    })
            }
        })
    },
    selectedAll: function(temp){
        var currentData = this;
        app.util.request({
            url:"entry/wxapp/getmyappointment",
            cachetime: "0",
            data:{
                user_id: this.user_id,
            },
            success: function(result){
                    console.log(result);
                    currentData.setData({
                        appointments: result.data
                    })
            }
        })
        currentData.setData({
            allAppointments: true,
            finishedAppointments: false,
            awaitingAppointments: false,
            hiddenAll: false,
            hiddenFinished: true,
            hiddenAwaiting: true,
        });
    },
    selectedFinished: function(temp){
        var currentData = this;
        app.util.request({
            url:"entry/wxapp/getmyfinishedappointment",
            cachetime: "0",
            data:{
                user_id: this.user_id,
            },
            success: function(result){
                    console.log(result);
                    currentData.setData({
                        appointments: result.data
                    })
            }
        })
        currentData.setData({
            allAppointments: false,
            finishedAppointments: true,
            awaitingAppointments: false,
            hiddenAll: true,
            hiddenFinished: false,
            hiddenAwaiting: true,
        });
    },
    selectedAwaiting: function(temp){
        var currentData = this;
        app.util.request({
            url:"entry/wxapp/getmyawaitingappointment",
            cachetime: "0",
            data:{
                user_id: this.user_id,
            },
            success: function(result){
                    console.log(result);
                    currentData.setData({
                        appointments: result.data
                    })
            }
        })
        currentData.setData({
            allAppointments: false,
            finishedAppointments: false,
            awaitingAppointments: true,
            hiddenAll: true,
            hiddenFinished: true,
            hiddenAwaiting: false,
        });
    },
})