var qqmapsdk, app = getApp(), Data = require("../../utils/util.js"), QQMapWX = require("../../../utils/qqmap-wx-jssdk.js");

Page({
    data: {
    },

    onLoad: function (t) {


        wx.login({

            success: function (tempLogin) {
                console.log('Weboost-JL: tempLogin', tempLogin);
                wx.setStorageSync("code", tempLogin.code);
                wx.getSetting({
                    success: function (tempGetSetting) {
                        console.log('Weboost-JL: tempGetSetting', tempGetSetting);

                    }
                })
            }
        })

    },
    bindGetUserInfo: function (temp) {
        wx.showLoading({
            title: "授权中",
            mask: true,
        })
        var currentData = this;
        wx.getUserInfo({
            success: function (t) {
                console.log('Weboost-JL: t', t);
                var code = wx.getStorageSync("code");
                console.log('Weboost-JL: code', code);
                wx.setStorageSync("user_info", t.userInfo);
                var a = t.userInfo.nickName,
                 n = t.userInfo.avatarUrl;
                app.util.request({
                    url: "entry/wxapp/openid",
                    cachetime: "0",
                    data: {
                        code: code
                    },
                    success: function (t) {
                        console.log(t),
                            wx.setStorageSync("key", t.data.session_key),
                            wx.setStorageSync("openid", t.data.openid);
                        var e = t.data.openid;
                        console.log('Weboost-JL: e', e);
                        app.util.request({
                            url: "entry/wxapp/Login",
                            cachetime: "0",
                            data: {
                                openid: e,
                                img: n,
                                name: a
                            },
                            success: function (t) {
                                console.log(t),
                                 currentData.setData({
                                    userinfo: t.data
                                }),
                                wx.setStorageSync("users", t.data);
                                wx.setStorageSync("uniacid", t.data.uniacid);
                               
                                wx.reLaunch({
                                    url:"../index/index"
                                })
                                wx.hideLoading({});
                            }
                        });
                    }
                });
            }
        })
    },


    onReady: function () { },
    onShow: function () { },
    onHide: function () { },
    onUnload: function () { },

    onReachBottom: function () {
        console.log("上拉触底");
    },
    onShareAppMessage: function () { },

});