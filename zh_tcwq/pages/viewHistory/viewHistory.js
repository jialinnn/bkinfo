var app = getApp();
Page({
    data: {
        
    },
    onLoad: function (result) {
        var currentData = this;
        var user_id = wx.getStorageSync("users").id;
        console.log('Weboost-JL: result', result);
        app.util.request({
            url: "entry/wxapp/getallunlockposts",
            cachetime: "0",
            data: {
                user_id: user_id
            },
            success: function (resultPosts) {
                console.log('Weboost-JL: resultPosts', resultPosts);
                currentData.setData({
                    allPosts: resultPosts.data
                })
            }
        })
    },
    see: function (temp) {
        var t = temp.currentTarget.dataset.id;
        wx.navigateTo({
            url: "../infodetial/infodetial?id=" + t,
            success: function (e) { },
            fail: function (e) { },
            complete: function (e) { }
        }); 
    },
    onShow: function() {
    },
})

