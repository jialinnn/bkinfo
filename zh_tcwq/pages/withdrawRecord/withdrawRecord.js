var util = require("../../utils/util.js"), app = getApp();

Page({
    data: {
        withdrawRecords: null,
    },

    selectedPending: function (temp) {
        var currentData = this;
        var currentUser = wx.getStorageSync("users");
        currentData.setData({ currentUser: currentUser });

        app.util.request({
            url: "entry/wxapp/MyTiXianPending",
            cachetime: "0",
            data: {
                user_id: currentUser.id,
            },
            success: function (res) {
                var withdrawRecords = res.data;
                withdrawRecords.forEach(withdrawRecord => {
                    withdrawRecord.formattedTime = app.ormatDate(withdrawRecord.time).slice(0, 16);
                    withdrawRecord.formattedShTime = app.ormatDate(withdrawRecord.sh_time).slice(0, 16);
                });

                console.log(withdrawRecords);
                currentData.setData({
                    withdrawRecords: withdrawRecords,
                    pending: true,
                    approval: false,
                    reject: false,
                    hiddenPending: false,
                    hiddenApproval: true,
                    hiddenReject: true,
                });
            }
        });
    },
    selectedApproval: function (temp) {
        var currentData = this;
        var currentUser = wx.getStorageSync("users");
        currentData.setData({ currentUser: currentUser });

        app.util.request({
            url: "entry/wxapp/MyTiXianApproval",
            cachetime: "0",
            data: {
                user_id: currentUser.id,
            },
            success: function (res) {
                var withdrawRecords = res.data;
                withdrawRecords.forEach(withdrawRecord => {
                    withdrawRecord.formattedTime = app.ormatDate(withdrawRecord.time).slice(0, 16);
                    withdrawRecord.formattedShTime = app.ormatDate(withdrawRecord.sh_time).slice(0, 16);
                });

                console.log(withdrawRecords);
                currentData.setData({
                    withdrawRecords: withdrawRecords,
                    pending: false,
                    approval: true,
                    reject: false,
                    hiddenPending: true,
                    hiddenApproval: false,
                    hiddenReject: true,
                });
            }
        });

    },
    selectedReject: function (temp) {
        var currentData = this;
        var currentUser = wx.getStorageSync("users");
        currentData.setData({ currentUser: currentUser });

        app.util.request({
            url: "entry/wxapp/MyTiXianReject",
            cachetime: "0",
            data: {
                user_id: currentUser.id,
            },
            success: function (res) {
                var withdrawRecords = res.data;
                withdrawRecords.forEach(withdrawRecord => {
                    withdrawRecord.formattedTime = app.ormatDate(withdrawRecord.time).slice(0, 16);
                    withdrawRecord.formattedShTime = app.ormatDate(withdrawRecord.sh_time).slice(0, 16);
                });

                console.log(withdrawRecords);
                currentData.setData({
                    withdrawRecords: withdrawRecords,
                    pending: false,
                    approval: false,
                    reject: true,
                    hiddenPending: true,
                    hiddenApproval: true,
                    hiddenReject: false,
                });
            }
        });

    },
    onLoad: function (options) {
        var page = this;
        var currentUser = wx.getStorageSync("users");
        page.setData({ currentUser: currentUser });

        app.util.request({
            url: "entry/wxapp/MyTiXian",
            cachetime: "0",
            data: {
                user_id: currentUser.id,
            },
            success: function (res) {
                var withdrawRecords = res.data;
                withdrawRecords.forEach(withdrawRecord => {
                    withdrawRecord.formattedTime = app.ormatDate(withdrawRecord.time).slice(0, 16);
                    withdrawRecord.formattedShTime = app.ormatDate(withdrawRecord.sh_time).slice(0, 16);
                });

                console.log(withdrawRecords);
                page.setData({
                    withdrawRecords: withdrawRecords,
                });
            }
        });
        page.selectedPending();
    }

});