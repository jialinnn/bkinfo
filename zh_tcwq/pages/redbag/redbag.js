var app = getApp();

Page({
    data: {
        page: 1,
        refresh_top: !1,
        store: [],
        price: 0,
        views: 0,
        givelike: 0,
        Congratulations: []
    },
    redinfo: function(a) {
        console.log(a);
        var t = a.currentTarget.dataset.id, e = a.currentTarget.dataset.logo;
        wx.navigateTo({
            url: "redinfo/redinfo?store_id=" + t + "&logo=" + e
        });
    },
    onLoad: function(a) {
        var e = this;
        app.util.request({
            url: "entry/wxapp/System",
            cachetime: "0",
            success: function(a) {
                console.log(a);
                var t = wx.getStorageSync("url");
                e.setData({
                    system: a.data,
                    url: t
                }), wx.setNavigationBarColor({
                    frontColor: "#ffffff",
                    backgroundColor: a.data.color,
                    animation: {
                        duration: 0,
                        timingFunc: "easeIn"
                    }
                });
            }
        }), e.reload();
    },
    reload: function(a) {
        var s = this;
        var d = s.data.page, l = s.data.store;
        console.log(d), app.util.request({
            url: "entry/wxapp/RedPaperList",
            cachetime: "0",
            data: {
                page: d,
                pagesize: 10
            },
            success: function(e) {
                console.log(e), e.data.length < 10 ? s.setData({
                    refresh_top: !0
                }) : s.setData({
                    refresh_top: !1,
                    page: d + 1
                }), l = function(a) {
                    for (var t = [], e = 0; e < a.length; e++) -1 == t.indexOf(a[e]) && t.push(a[e]);
                    return t;
                }(l = l.concat(e.data));
                var o = Number(s.data.price), n = Number(s.data.views), r = Number(s.data.givelike), i = s.data.Congratulations, a = function(t) {
                    n += Number(e.data[t].views), r += Number(e.data[t].hbfx_num), console.log(e.data[t].details), 
                    e.data[t].img = e.data[t].img.split(","), 4 <= e.data[t].img.length ? e.data[t].img = e.data[t].img.splice(0, 4) : e.data[t].img = e.data[t].img, 
                    1 == e.data[t].hb_random ? e.data[t].hb_money = Number(e.data[t].hb_money) : e.data[t].hb_money = (Number(e.data[t].hb_money) * Number(e.data[t].hb_num)).toFixed(2), 
                    o += Number(e.data[t].hb_money), app.util.request({
                        url: "entry/wxapp/HongList",
                        cachetime: "0",
                        data: {
                            id: e.data[t].id
                        },
                        success: function(a) {
                            0 < a.data.length && (i = i.concat(a.data)), console.log(a.data, i), Number(e.data[t].hb_num) <= a.data.length ? e.data[t].rob = !1 : e.data[t].rob = !0, 
                            console.log(l), s.setData({
                                store: l,
                                Congratulations: i,
                                price: o.toFixed(2),
                                views: n,
                                givelike: r
                            });
                        }
                    });
                };
                for (var t in e.data) a(t);
            }
        });
    },
    onReady: function() {},
    onShow: function() {},
    onHide: function() {},
    onUnload: function() {},
    onPullDownRefresh: function() {
        this.setData({
            page: 1,
            refresh_top: !1,
            store: [],
            price: 0,
            views: 0,
            givelike: 0
        }), this.reload(), wx.stopPullDownRefresh();
    },
    onReachBottom: function() {
        console.log("上拉触底"), 0 == this.data.refresh_top ? this.reload() : console.log("没有更多了");
    },
    onShareAppMessage: function() {}
});