var app = getApp();

Page({
    data: {
        credit: 0,
        user_id:0,
    },
    onLoad: function(temp) {
        var currentData = this;
        var currentUser = wx.getStorageSync("users");
        this.credit = temp.credit;
        this.user_id = currentUser.id
        console.log(currentUser);
        console.log(temp);
        currentData.setData({
            credit: this.credit,
        })
    },
    finishCharge: function(temp){
        wx.reLaunch({
            url:"../logs/logs",
        })
    }

});