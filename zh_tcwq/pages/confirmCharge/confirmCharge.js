var app = getApp();

Page({
  data: {
    credit: "",
    user_id: 0,
    qrcode: ""
  },
  onLoad: function(temp) {
    var currentData = this;
    var currentUser = wx.getStorageSync("users");
    this.credit = temp.credit;
    this.user_id = currentUser.id
    console.log(currentUser);
    console.log(temp);
    currentData.setData({
      credit: temp.credit,
    });

  },
  gotohelp: function() {

    wx.navigateTo({
      url:  "./help",
      success: function(res) {},
      fail: function(res) {},
      complete: function(res) {},
    })
  },


  confirmCredit: function(temp) {
    var that = this;
    console.log(app.siteInfo.uniacid);
    var credit = this.credit;
    wx.showLoading({
      title: "进行中。。。",
      mask: true,
    })
    app.util.request({
      url: "entry/wxapp/getqrcode",
      cachetime: "0",
      // method: 'post',
      data: {
        action: "pay",
        uniacid: app.siteInfo.uniacid,
        //sub_openid: wx.getStorageSync("openid"),
        //increment_id: this.increment_id,
        subject: "BKINFO加值：$" + this.credit,
        grandtotal: parseFloat(this.credit),
        //payType: this.payType,
        payment_channels: "WECHAT",
        sandbox: 1,
        success_return_url: app.siteInfo.siteroot +
          "payment/epayments/wxPayNotify.php?module=zh_tcwq&version_id=27&credit=" + credit +
          "&user_id=" + this.user_id +
          "&uniacid=" + app.siteInfo.uniacid +
          "&recharge_currency=" + wx.getStorageSync("System").recharge_currency,
      },
      success: function(data) {
        console.log(data);
        if (data.data.qr_code_url[0].code == "0") {

          that.setData({
            qrcode: data.data.qr_url[0],
          });

        } else {
          that.setData({
            qrcode: "",
          });

        }
      },
      fail: function(data) {
        console.log(data)
      },
      complete: function(data) {
        //console.log(data)
      },
    });
    /*
            app.util.request({
                url: "entry/wxapp/chargecredit",
                cachetime: "0",
                data: {
                    user_id: this.user_id,
                    credit: credit
                },
                success: function (temp) {
                    console.log(temp);
                    var serialDate = new Date();
                    var serialYear = serialDate.getFullYear();
                    var serialMonth = serialDate.getMonth() + 1;
                    if (serialMonth < 10) {
                        serialMonth = 0 + "" + serialMonth;
                    }
                    var serialDay = serialDate.getDate();
                    if (serialDay < 10) {
                        serialDay = 0 + "" + serialDay;
                    }
                    var serialHour = serialDate.getHours();
                    if (serialHour < 10) {
                        serialHour = 0 + "" + serialHour;
                    }
                    var serialMins = serialDate.getMinutes();
                    if (serialMins < 10) {
                        serialMins = 0 + "" + serialMins;
                    }
                    var serialNum = serialYear + "" + serialMonth + "" + serialDay + "" + serialHour + "" + serialMins;
                    var date = serialYear + "-" + serialMonth + "-" + serialDay;
                    var time = serialHour + ":" + serialMins;
                    console.log(serialNum);
                    var user_id = wx.getStorageSync("users").id;
                    wx.hideLoading();
                    app.util.request({
                        url: "entry/wxapp/recordcredithistory",
                        cachetime: "0",
                        data: {
                            user_id: user_id,
                            credit: credit,
                            date: date,
                            time: time,
                            serial_num: serialNum,
                        },
                        success: function (recordAdded) {
                            console.log(recordAdded);
                            wx.hideLoading({});
                            wx.navigateTo({
                                url: "chargeSuccess?credit=" + credit,
                            })
                        }

                    })
                    
                },
            });
    */
  },

  pay: function (t) {
    var e = this,
      a = wx.getStorageSync("openid");
      //r = t.currentTarget.dataset.id,
      //o = t.currentTarget.dataset.storeid;
    //console.log(o);
    var n = this.credit;
    app.util.request({
      url: "entry/wxapp/Pay",
      cachetime: "0",
      data: {
        openid: a,
        money: n,
        //order_id: r
      },
      success: function (t) {
        console.log(t);
        if (t.data.code == 0) {
          wx.requestPayment({
            timeStamp: t.data.timeStamp,
            nonceStr: t.data.nonceStr,
            package: t.data.package,
            signType: t.data.signType,
            paySign: t.data.paySign,
            success: function (t) {
              wx.showToast({
                title: "支付成功",
                duration: 3e3,
                complete: function(t) {
                  wx.navigateBack({//返回
                    delta: 2
                  })
                }
              });
              // console.log("这里是支付成功"),
              //   console.log(t),
              //   app.util.request({
              //     url: "entry/wxapp/PayOrder",
              //     cachetime: "0",
              //     data: {
              //       order_id: r
              //     },
              //     success: function (t) {
              //       console.log("改变订单状态"), console.log(t), e.refresh();
              //     }
              //   }),
              //   app.util.request({
              //     url: "entry/wxapp/sms2",
              //     cachetime: "0",
              //     data: {
              //       store_id: o
              //     },
              //     success: function (t) {
              //       console.log(t);
              //     }
              //   });
            },
            fail: function (t) {
              wx.showToast({
                icon: 'none',
                title: "支付失败",
                duration: 3e3
              });
            }
          });
        }
        else {
          wx.showToast({
            icon: 'none',
            title: t.data.error_msg,
            duration: 3e3
          });
        }
      }
    });
  },
});