var app = getApp();

Page({
    data: {
        sliderOffset: 0,
        activeIndex1: 1,
        sliderLeft: 35,
        refresh_top: !1,
        refresh1_top: !1,
        page: 1,
        page1: 1,
        tie: [],
        tie1: [],
        allArticles: true,
        todayArticles: false,
        famousArticles: false,
        hiddenAll: false,
        hiddenToday: true,
        hiddenFamous: true,
        bgColor: "",
        showModal: false,
        article_id: "",
        goToCharge: false,
        goToChargeButton: false,
        showCreditSelect: false,
        credit: {},
        subtype:0
    },

    selectedAll: function () {
        var currentData = this;
        var user_id = wx.getStorageSync("users").id;
        var classId = wx.getStorageSync("classId");
        var subclass = currentData.data.subtype;
        console.log("zhege id: " + classId);
        console.log("zhege subid: " + subclass);
        app.util.request({
            url: "entry/wxapp/getallposts",
            cachetime: "0",
            data: {
                uniacid: 29,
                type_id: classId,
                subclass: subclass,
                user_id: user_id,
            },
            success: function (resultPosts) {
                console.log(resultPosts);
                resultPosts.data.forEach(post => {
                    post.formattedTime = currentData.ormatDate(post.time).slice(0, 16);
                });
                console.log(resultPosts.data);
                currentData.setData({
                    allPosts: resultPosts.data,
                    allArticles: true,
                    todayArticles: false,
                    famousArticles: false,
                    hiddenAll: false,
                    hiddenToday: true,
                    hiddenFamous: true,
                })
            }
        })
    },
    selectedToday: function () {
        var currentData = this;
        var user_id = wx.getStorageSync("users").id;
        var classId = wx.getStorageSync("classId");
        var subclass = currentData.data.subtype;
        console.log("zhege id2: " + classId);
        app.util.request({
            url: "entry/wxapp/gettodayposts",
            cachetime: "0",
            data: {
                uniacid: 29,
                type_id: classId,
                subclass: subclass,
                user_id: user_id,
            },
            success: function (resultPosts) {
                resultPosts.data.forEach(post => {
                    post.formattedTime = currentData.ormatDate(post.time).slice(0, 16);
                });
                console.log(resultPosts.data);
                currentData.setData({
                    allPosts: resultPosts.data,
                    allArticles: false,
                    todayArticles: true,
                    famousArticles: false,
                    hiddenAll: true,
                    hiddenToday: false,
                    hiddenFamous: true,
                })
            }
        })
    },
    selectedFamous: function () {
        var currentData = this;
        var user_id = wx.getStorageSync("users").id;
        var classId = wx.getStorageSync("classId");
        var subclass = currentData.data.subtype;
        app.util.request({
            url: "entry/wxapp/getfamousposts",
            cachetime: "0",
            data: {
                uniacid: 29,
                type_id: classId,
                subclass: subclass,
                user_id: user_id,
            },
            success: function (resultPosts) {
                resultPosts.data.forEach(post => {
                    post.formattedTime = currentData.ormatDate(post.time).slice(0, 16);
                });
                console.log(resultPosts.data);
                currentData.setData({
                    allPosts: resultPosts.data,
                    allArticles: false,
                    todayArticles: false,
                    famousArticles: true,
                    hiddenAll: true,
                    hiddenToday: true,
                    hiddenFamous: false,
                })
            }
        })
    },
    hdsy: function (t) {
        wx.switchTab({
            url: "../index/index",
            success: function (t) { },
            fail: function (t) { },
            complete: function (t) { }
        });
    },
    hdft: function (t) {
        wx.navigateTo({
            url: "../fabu/fabu/fabu",
            success: function (t) { },
            fail: function (t) { },
            complete: function (t) { }
        });
    },
    tabClick: function (t) {
        var that = this;
        console.log("ggggggggggg: ",that.data.subtype);
        that.data.subtype = t.currentTarget.dataset.class;
        console.log("awdasfarfawrafaf: ",that.data.subtype);
        var a = t.currentTarget.id, e = this.data.classification, i = e[a].id, n = e[a].name;
        that.setData({
            activeIndex: a,
            activeIndex1: 0,
            page1: 1,
            type2_id: i,
            type2_name: n,
            tie1: [],
            allArticles: false,
            todayArticles: false,
            famousArticles: false,
            hiddenAll: true,
            hiddenToday: true,
            hiddenFamous: true,
        }), that.refresh1();
    },
    getUnlockIntegral: function(temp){
      var that = this;
      app.util.request({
         url:"entry/wxapp/GetUnlockIntegral",
         cachetime: "0",
         data:{},
         success: function(result){
             console.log(result);
             wx.setStorageSync("unlockIntegral",result.data.unlock_integral);
             that.setData({
                 unlockIntegral: result.data.unlock_integral
             })
         }
      });
    },
    onLoad: function (t) {
        console.log(t);
        var a = this;
        wx.setStorageSync("classId", t.id);
        var bgColor = wx.getStorageSync("bgColor");
        console.log(bgColor);
        wx.setNavigationBarColor({
            frontColor: "#ffffff",
            backgroundColor: wx.getStorageSync("color"),
            animation: {
                duration: 0,
                timingFunc: "easeIn"
            }
        }), wx.getSystemInfo({
            success: function (t) {
                a.setData({
                    windowHeight: t.windowHeight
                });
            }
        }), wx.setNavigationBarTitle({
            title: t.name
        });
        var e = t.id, i = wx.getStorageSync("url");
        a.setData({
            id: e,
            url: i,
            tname: t.name,
            bgColor: bgColor,
        }), a.reload(), a.refresh();

    },
    wole: function (t) {
        var that = this;
        that.data.subtype = 0;
        that.setData({
            activeIndex: -1,
            activeIndex1: 1,
            classification_info: this.data.tie
        });
    },
    //function that gets all the posts;


    reload: function (t) {
        var a = this, e = a.data.id;
        console.log(e), app.util.request({
            url: "entry/wxapp/type2",
            cachetime: "0",
            data: {
                id: e
            },
            success: function (t) {
                console.log("type2: ",t);
                if (console.log(t), 0 < t.data.length) {
                    t.data[0].id, t.data[0].name;
                    a.setData({
                        classification: t.data
                    });
                }
            }
        });
    },
    refresh: function (t) {
        var o = this, a = o.data.id, e = wx.getStorageSync("city");
        console.log(e), console.log(o.data.page), app.util.request({
            url: "entry/wxapp/list",
            cachetime: "0",
            data: {
                type_id: a,
                page: o.data.page,
                cityname: e
            },
            success: function (t) {
                if (console.log(t), 0 == t.data.length) o.setData({
                    refresh_top: !0
                }); else {
                    o.setData({
                        page: o.data.page + 1
                    });
                    var a = o.data.tie;
                    for (var e in a = a.concat(t.data), t.data) {
                        for (var i in t.data[e].tz.img = t.data[e].tz.img.split(","), t.data[e].tz.details = t.data[e].tz.details.replace("↵", " "),
                            t.data[e].tz.time = o.ormatDate(t.data[e].tz.time), 4 < t.data[e].tz.img.length && (t.data[e].tz.img_length = Number(t.data[e].tz.img.length) - 4),
                            4 <= t.data[e].tz.img.length ? t.data[e].tz.img = t.data[e].tz.img.slice(0, 4) : t.data[e].tz.img = t.data[e].tz.img,
                            t.data[e].label) t.data[e].label[i].number = (void 0, n = "rgb(" + Math.floor(255 * Math.random()) + "," + Math.floor(255 * Math.random()) + "," + Math.floor(255 * Math.random()) + ")",
                                n);
                    }
                    o.setData({
                        classification_info: a,
                        tie: a
                    });
                    console.log(a);
                }
                var n;
            }
        });
    },
    refresh1: function (t) {
        var r = this, a = wx.getStorageSync("city");
        console.log(r.data.type2_id), console.log(r.data.type2_name), app.util.request({
            url: "entry/wxapp/PostList",
            cachetime: "0",
            data: {
                type2_id: r.data.type2_id,
                page: r.data.page1,
                cityname: a
            },
            success: function (t) {
                console.log(t), 0 == t.data ? (wx.showToast({
                    title: "没有更多了",
                    icon: "",
                    image: "",
                    duration: 1e3,
                    mask: !0,
                    success: function (t) { },
                    fail: function (t) { },
                    complete: function (t) { }
                }), r.setData({
                    refresh1_top: !0
                })) : r.setData({
                    page1: r.data.page1 + 1
                });
                var a, e = r.data.tie1;
                for (var i in console.log(e), e = e.concat(t.data), t.data) {
                    t.data[i].tz.type2_name = r.data.type2_name;
                    var n = r.ormatDate(t.data[i].tz.time);
                    for (var o in t.data[i].tz.time = n.slice(0, 16), t.data[i].tz.img = t.data[i].tz.img.split(",").slice(0, 4),
                        t.data[i].label) t.data[i].label[o].number = (void 0, a = "rgb(" + Math.floor(255 * Math.random()) + "," + Math.floor(255 * Math.random()) + "," + Math.floor(255 * Math.random()) + ")",
                            a);
                }
                r.setData({
                    classification_info: e,
                    tie1: e
                });
            }
        });
    },
    EventHandle: function (t) {
        1 == this.data.activeIndex1 ? 0 == this.data.refresh_top && this.refresh() : 0 == this.data.refresh1_top && this.refresh1();
    },
    thumbs_up: function (t) {
        var a = this, e = t.currentTarget.dataset.id, i = wx.getStorageSync("users").id, n = Number(t.currentTarget.dataset.num);
        app.util.request({
            url: "entry/wxapp/Like",
            cachetime: "0",
            data: {
                information_id: e,
                user_id: i
            },
            success: function (t) {
                1 != t.data ? wx.showModal({
                    title: "提示",
                    content: "不能重复点赞",
                    showCancel: !0,
                    cancelText: "取消",
                    cancelColor: "",
                    confirmText: "确认",
                    confirmColor: "",
                    success: function (t) { },
                    fail: function (t) { },
                    complete: function (t) { }
                }) : (a.reload(), a.setData({
                    thumbs_ups: !0,
                    thumbs_up: n + 1
                }));
            }
        });
    },
    ormatDate: function (t) {
        var a = new Date(1e3 * t);
        return a.getFullYear() + "-" + e(a.getMonth() + 1, 2) + "-" + e(a.getDate(), 2) + " " + e(a.getHours(), 2) + ":" + e(a.getMinutes(), 2) + ":" + e(a.getSeconds(), 2);
        function e(t, a) {
            for (var e = "" + t, i = e.length, n = "", o = a; o-- > i;) n += "0";
            return n + e;
        }
    },
    see: function (t) {
        wx.navigateTo({
            url: "../infodetial/infodetial?id=" + t.currentTarget.dataset.id,
            success: function (t) { },
            fail: function (t) { },
            complete: function (t) { }
        });
    },
    cancelModal: function (temp) {
        var currentThis = this;
        currentThis.setData({
            showModal: false,
        })
    },
    goToLogPage: function (temp) {
        var page = this;

        page.setData({showModal: false});
        wx.navigateTo({
            url: "../chargeCredit/chargeCredit"
        })
    },
    unlockCurrentPost: function (temp) {
        var currentThis = this;
        var user_id = wx.getStorageSync("users").id;
        var article_id = currentThis.article_id;
        console.log('Weboost-JL: article_id', article_id);
        app.util.request({
            url: "entry/wxapp/getcredit",
            cachetime: "0",
            data: {
                user_id: user_id,
            },
            success: function (resultNumber) {
                var currentData = this;
                console.log(currentData);
                console.log(resultNumber);
                if (resultNumber.data.money < 50) {
                    currentThis.setData({
                        goToCharge: true,
                        goToChargeButton: true,
                    })
                    // wx.showModal({
                    //     title: "积分不足",
                    //     content: "积分不足50无法解锁",
                    //     confirmText: "去充值",
                    //     cancelColor: "#ff0000",
                    //     success: function (temp) {
                    //         if (temp.confirm) {
                    //             wx.reLaunch({
                    //                 url: "../logs/logs"
                    //             })
                    //         } else if (temp.cancel) {
                    //             console.log("暂不充值");
                    //         }
                    //     }
                    // })
                } else {
                    currentThis.setData({
                        goToCharge: false,
                        goToChargeButton: false,
                    })
                    app.util.request({
                        url: "entry/wxapp/creditdeduct",
                        cachetime: "0",
                        data: {
                            user_id: user_id,
                            bounty: parseInt(wx.getStorageSync("unlockIntegral")),
                        },
                        success: function (temp) {
                            console.log(temp);
                            app.util.request({
                                url: "entry/wxapp/unlockcurrentarticle",
                                cachetime: "0",
                                data: {
                                    article_id: article_id,
                                    user_id: user_id,
                                },
                                success: function (temp) {

                                    currentThis.setData({
                                        showModal: false
                                    })
                                    wx.navigateTo({
                                        url: "../infodetial/infodetial?id=" + article_id,
                                        success: function (t) { },
                                        fail: function (t) { },
                                        complete: function (t) { }
                                    });
                                }
                            })
                        }
                    });
                }
            }
        });
    },
    notUnlockYet: function (event) {
        var page = this;

        page.setData({
            article_id: event.currentTarget.dataset.id,
        });
        var currentUser = wx.getStorageSync("users");

        app.util.request({
            url: "entry/wxapp/getcredit",
            cachetime: "0",
            data: {
                user_id: currentUser.id,
            },
            success: function (res) {
                page.setData({
                    showModal: true,
                    credit: res.data,
                });
                if (currentUser.responder) {
                    page.setData({showCreditSelect: true});
                } else {
                    if (page.data.credit.userCredit >= 50) {
                        page.setData({
                            showCreditSelect: false,
                            comfirmCredit: true,
                        });
                    } else {
                        page.unlockPost({target: {dataset: {creditType: "user"}}});
                    }
                }
            }
        });
    },

    unlockPost: function (event) {
        var page = this;

        var creditType = event.target.dataset.creditType;
        if ((creditType === "responder" && page.data.credit.responderCredit < 50)
            || (creditType === "user" && page.data.credit.userCredit < 50)) {
            page.setData({
                showCreditSelect: false,
                comfirmCredit: false,
            });
            return;
        }

        app.util.request({
            url: "entry/wxapp/unlockarticle",
            cachetime: "0",
            data: {
                user_id: wx.getStorageSync("users").id,
                article_id: page.data.article_id,
                creditType: creditType,
                bounty: parseInt(wx.getStorageSync("unlockIntegral")),
            },
            success: function (res) {
                page.setData({
                    showModal: false
                });
                wx.navigateTo({
                    url: "../infodetial/infodetial?id=" + page.data.article_id,
                    success: function (t) { },
                    fail: function (t) { },
                    complete: function (t) { }
                });
            }
        });
    },
    // notUnlockYet: function(temp){
    //     console.log(temp);
    //     var article_id = temp.currentTarget.dataset.id;
    //     console.log(article_id);
    //     wx.showModal({
    //         title: "文章未解锁",
    //         content: "是否要花费50积分解锁改文章",
    //         confirmText: "去解锁",
    //         cancelColor: "#ff0000",
    //         success: function(temp){
    //             if(temp.confirm){
    //                 console.log("去解锁");
    //                 var user_id = wx.getStorageSync("users").id;
    //                 app.util.request({
    //                     url:"entry/wxapp/getcredit",
    //                     cachetime: "0",
    //                     data:{
    //                         user_id: user_id,
    //                     },
    //                     success: function(resultNumber){
    //                         var currentData = this;
    //                         console.log(currentData);
    //                         console.log(resultNumber);
    //                         if(resultNumber.data.money < 50){
    //                             wx.showModal({
    //                                 title: "积分不足",
    //                                 content: "积分不足50无法解锁",
    //                                 confirmText: "去充值",
    //                                 cancelColor: "#ff0000",
    //                                 success: function(temp){
    //                                     if(temp.confirm){
    //                                         wx.reLaunch({
    //                                             url: "../logs/logs"
    //                                         })
    //                                     }else if(temp.cancel){
    //                                         console.log("暂不充值");
    //                                     }
    //                                 }
    //                             })
    //                         }else{
    //                             app.util.request({
    //                                 url: "entry/wxapp/creditdeduct",
    //                                 cachetime: "0",
    //                                 data: {
    //                                     user_id: user_id,
    //                                     bounty: 50,
    //                                 },
    //                                 success: function (temp) {
    //                                 console.log(temp);
    //                                 app.util.request({
    //                                     url:"entry/wxapp/unlockcurrentarticle",
    //                                     cachetime: "0",
    //                                     data:{
    //                                         article_id: article_id,
    //                                         user_id: user_id,
    //                                     },
    //                                     success:function(temp){
    //                                         wx.showModal({
    //                                             title:"提示",
    //                                             content:"解锁成功",
    //                                             success: function(temp){
    //                                                 wx.navigateTo({
    //                                                     url: "../infodetial/infodetial?id=" + article_id,
    //                                                     success: function(t) {},
    //                                                     fail: function(t) {},
    //                                                     complete: function(t) {}
    //                                                 });
    //                                             }
    //                                         })

    //                                     }
    //                                 })
    //                                 }
    //                             });


    //                         }
    //                     }
    //                 })
    //             }else if(temp.cancel){
    //                 console.log("暂不解锁");
    //             }
    //         }

    //     })
    // },
    phone: function (t) {
        var a = t.currentTarget.dataset.id;
        wx.makePhoneCall({
            phoneNumber: a
        });
    },
    onReady: function () { },
    onShow: function () {
        var currentData = this;
        currentData.selectedAll();
        currentData.getUnlockIntegral();
    },
    onHide: function () { },
    onUnload: function () { },
    onPullDownRefresh: function () { },
    onReachBottom: function () { },
    onShareAppMessage: function () {
        // var t = this.data.id, a = this.data.tname;
        // return console.log(t, a), {
        //     path: "/zh_tcwq/pages/marry/marry?id=" + t + "&name=" + a,
        //     success: function(t) {},
        //     fail: function(t) {}
        // };
        var user_id = wx.getStorageSync("users").id;
        return {
            success: function (res) {
                app.util.request({
                    url: "entry/wxapp/chargecredit",
                    cachetime: "0",
                    data: {
                        user_id: user_id,
                        credit: 0.1
                    },
                    success: function (temp) {
                        console.log(temp);
                        var serialDate = new Date();
                        var serialYear = serialDate.getFullYear();
                        var serialMonth = serialDate.getMonth() + 1;
                        if (serialMonth < 10) {
                            serialMonth = 0 + "" + serialMonth;
                        }
                        var serialDay = serialDate.getDate();
                        if (serialDay < 10) {
                            serialDay = 0 + "" + serialDay;
                        }
                        var serialHour = serialDate.getHours();
                        if (serialHour < 10) {
                            serialHour = 0 + "" + serialHour;
                        }
                        var serialMins = serialDate.getMinutes();
                        if (serialMins < 10) {
                            serialMins = 0 + "" + serialMins;
                        }
                        var serialNum = serialYear + "" + serialMonth + "" + serialDay + "" + serialHour + "" + serialMins;
                        var date = serialYear + "-" + serialMonth + "-" + serialDay;
                        var time = serialHour + ":" + serialMins;
                        console.log(serialNum);
                        var user_id = wx.getStorageSync("users").id;
                        wx.hideLoading();
                        app.util.request({
                            url: "entry/wxapp/testshareappmessage",
                            cachetime: "0",
                            data: {
                                user_id: user_id,
                                credit: 0.1,
                                date: date,
                                time: time,
                                serial_num: serialNum,
                            },
                            success: function (recordAdded) {
                                console.log(recordAdded);
                                wx.hideLoading({});
                            }

                        })

                    },
                });
            },

        }
    }
});