var app = getApp();

Page({
    data: {
        user_id:"",
    },
    onLoad: function(temp) {
        var user_id = temp.id;
        this.user_id = user_id;
        var currentData = this;
        console.log(currentData);
        console.log(user_id);
        console.log(temp);
        currentData.setData({
            allPayments: true,
            finishedPayments: false,
            awaitingPayments: false,
            hiddenAll: false,
            hiddenFinished: true,
            hiddenAwaiting: true,
        });
        app.util.request({
            url:"entry/wxapp/getcredithistory",
            cachetime:"0",
            data:{
                user_id : user_id,
            },
            success: function(creditResult){
                console.log(creditResult);
                currentData.setData({
                    credits: creditResult.data,
                })
            }
        })
    },
    selectedAll: function(temp){
        var currentData = this;
        app.util.request({
            url:"entry/wxapp/getcredithistory",
            cachetime:"0",
            data:{
                user_id : this.user_id,
            },
            success: function(creditResult){
                console.log(creditResult);
                currentData.setData({
                    credits: creditResult.data,
                })
            }
        })
        currentData.setData({
            allPayments: true,
            finishedPayments: false,
            awaitingPayments: false,
            hiddenAll: false,
            hiddenFinished: true,
            hiddenAwaiting: true,
        });
    },
    selectedFinished: function(temp){
        var currentData = this;
        app.util.request({
            url:"entry/wxapp/getfinishedcredithistory",
            cachetime:"0",
            data:{
                user_id : this.user_id,
            },
            success: function(creditResult){
                console.log(creditResult);
                currentData.setData({
                    credits: creditResult.data,
                })
            }
        })
        currentData.setData({
            allPayments: false,
            finishedPayments: true,
            awaitingPayments: false,
            hiddenAll: true,
            hiddenFinished: false,
            hiddenAwaiting: true,
        });
    },
    selectedAwaiting: function(temp){
        var currentData = this;
        app.util.request({
            url:"entry/wxapp/getawaitingcredithistory",
            cachetime:"0",
            data:{
                user_id : this.user_id,
            },
            success: function(creditResult){
                console.log(creditResult);
                currentData.setData({
                    credits: creditResult.data,
                })
            }
        })
        currentData.setData({
            allPayments: false,
            finishedPayments: false,
            awaitingPayments: true,
            hiddenAll: true,
            hiddenFinished: true,
            hiddenAwaiting: false,
        });
    },
});