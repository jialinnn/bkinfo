  var app = getApp();

Page({
    data: {
        
    },
    onLoad: function(temp) {
        wx.login({
            success: function(tempLogin){
            console.log('Weboost-JL: tempLogin', tempLogin);
            wx.getSetting({
                success: function(tempGetSetting){
                console.log('Weboost-JL: tempGetSetting', tempGetSetting);
                    if(tempGetSetting.authSetting["scope.userInfo"] != true){
                        wx.navigateTo({
                            url: "../authorization/authorization"
                        })
                    }
                }
            })
            }
        })
        var currentData = this;
        var currentUser = wx.getStorageSync("users");
        console.log(currentUser);
        
    },
    detailButton: function(temp){
        var user = wx.getStorageSync("users");
        wx.navigateTo({
            url:"creditHistory?id=" + user.id,
        })
    },
    tenCredit:function(temp){
        var currentData = this;
        var currentUser = wx.getStorageSync("users");
        var user_id = currentUser.id;
        var credit = 10;
        wx.navigateTo({
            url:"../confirmCharge/confirmCharge?id=" + user_id + "&credit="+ credit,
            success: function(n) {},
            fail: function(n) {},
            complete: function(n) {}
        })
    },
    twentyCredit: function(temp){
        var currentData = this;
        var currentUser = wx.getStorageSync("users");
        var user_id = currentUser.id;
        var credit = 30;
        wx.navigateTo({
            url:"../confirmCharge/confirmCharge?id=" + user_id + "&credit="+ credit,
            success: function(n) {},
            fail: function(n) {},
            complete: function(n) {}
        })
    },
    thirtyCredit: function(temp){
        var currentData = this;
        var currentUser = wx.getStorageSync("users");
        var user_id = currentUser.id;
        var credit = 50;
        wx.navigateTo({
            url:"../confirmCharge/confirmCharge?id=" + user_id + "&credit="+ credit,
            success: function(n) {},
            fail: function(n) {},
            complete: function(n) {}
        })
    }
});