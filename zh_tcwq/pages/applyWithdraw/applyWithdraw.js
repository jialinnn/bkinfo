var util = require("../../utils/util.js"), app = getApp();

Page({
    data: {
        currentUser: null,
        credit: null,
        currentWithdrawType: "bank",
        withdrawRatio: null,
        disableSubmit: false,
        inputCredit: 0,
        income: 0,
    },

    onLoad: function (option) {
        var page = this;

        var currentUser = wx.getStorageSync("users");
        page.setData({currentUser: currentUser});

        app.util.request({
            url: "entry/wxapp/getcredit",
            cachetime: "0",
            data: {
                user_id: currentUser.id,
            },
            success: function (res) {
                page.setData({
                    credit: res.data.responderCredit,
                });
            }
        });

        var system = wx.getStorageSync("System");
        page.setData({
            inputCredit: 0,
            income: 0,
            minPrice: system.tx_money,
            withdrawRatio: system.tx_currency,
        });
    },

    changeWithdrawType: function (event) {
        var page = this;
        page.setData({currentWithdrawType: event.detail.value});
    },

    bindblur: function (e) {
        var page = this;

        var inputCredit = Number(e.detail.value);
        var availableCredit = Number(page.data.credit);
        console.log(inputCredit, availableCredit);
        if (availableCredit < inputCredit) {
            wx.showModal({
                title: "提示",
                content: "您的提积分额超出可提现积分"
            });
            page.setData({
                disableSubmit: true,
                inputCredit: 0,
                income: 0,
            });
            return;
        }

        if (inputCredit <= 0) {
            page.setData({
                disableSubmit: true,
                inputCredit: 0,
                income: 0,
            });
            return;
        }

        page.setData({disableSubmit: false});
        var income = inputCredit / page.data.withdrawRatio;
        page.setData({
            inputCredit: inputCredit,
            income: Number(income).toFixed(2),
        });
    },

    submit: function (event) {
        var page = this;

        var formValue = event.detail.value;

        if (formValue.withdrawCredit === "") {
            wx.showModal({
                content: '请输入要提现的积分',
                showCancel: false,
            });
            return;
        }
        var withdrawCredit = parseInt(page.data.inputCredit);
        if (withdrawCredit > page.data.credit) {
            wx.showModal({
                title: '您的积分不足',
                content: '提现积分不能高于您的专家积分',
                showCancel: false,
            });
            return;
        }

        var data = {
            tx_cost: withdrawCredit,
            sj_cost: page.data.income,
            user_id: page.data.currentUser.id,
            store_id: page.data.currentUser.responder.id,
            method: 2,
        };

        if (page.data.currentWithdrawType === "bank") {
            if (formValue.bankName === "" || formValue.accountName === "" || formValue.bsbNum === "" || formValue.accountNum === "") {
                wx.showModal({
                    content: '请输入您的账号信息',
                    showCancel: false,
                });
                return;
            }

            data.type = 3;
            data.name = formValue.accountName;
            data.bank = formValue.bankName;
            data.username = "BSB: " + formValue.bsbNum + ", Account number: " + formValue.accountNum;

        } else if (page.data.currentWithdrawType === "alipay") {
            if (formValue.aplipayNum === "") {
                wx.showModal({
                    content: '请输入您的账号信息',
                    showCancel: false,
                });
                return;
            }

            data.type = 1;
            data.username = formValue.aplipayNum;

        } else if (page.data.currentWithdrawType === "wechatpay") {
            if (formValue.wechatpayNum === "") {
                wx.showModal({
                    content: '请输入您的账号信息',
                    showCancel: false,
                });
                return;
            }

            data.type = 2;
            data.username = formValue.wechatpayNum;

        } else {
            return;
        }

        console.log(data);
        app.util.request({
            url: "entry/wxapp/TiXian",
            cachetime: "0",
            data: data,
            success: function (res) {
                console.log(res);
                wx.showModal({
                    title: '申请提交成功',
                    // content: '',
                    showCancel: false,
                    success: function (event) {
                        page.onLoad();
                    },
                });
            },
            fail: function (error) {
                wx.showModal({
                    title: '申请提交失败',
                    // content: '',
                    showCancel: false,
                    success: function (event) {
                        page.onLoad();
                    },
                });
            }
        })

    },

});