var app = getApp();
Page({
    data: {
        keywords:""
    },
    onLoad: function (result) {
        var currentData = this;
        this.keywords = result.keywords;
        console.log('Weboost-JL: result', result);
        var user_id = wx.getStorageSync("users").id;
        app.util.request({
            url: "entry/wxapp/getallsearchposts",
            cachetime: "0",
            data: {
                keywords: result.keywords,
                user_id: user_id,
                uniacid: 29
            },
            success: function (resultPosts) {
                console.log('Weboost-JL: resultPosts', resultPosts);
                currentData.setData({
                    allPosts: resultPosts.data
                })
            }
        })
    },
    refresh:function(temp){
        var currentData = this;
        var user_id = wx.getStorageSync("users").id;
        app.util.request({
            url: "entry/wxapp/getallsearchposts",
            cachetime: "0",
            data: {
                keywords: this.keywords,
                user_id: user_id,
                uniacid: 29
            },
            success: function (resultPosts) {
                console.log('Weboost-JL: resultPosts', resultPosts);
                currentData.setData({
                    allPosts: resultPosts.data
                })
            }
        })
    },
    see: function (temp) {
        var t = temp.currentTarget.dataset.id;
        wx.navigateTo({
            url: "../infodetial/infodetial?id=" + t,
            success: function (e) { },
            fail: function (e) { },
            complete: function (e) { }
        }); 
    },

    notUnlockYet: function(temp){
        console.log(temp);
        var article_id = temp.currentTarget.dataset.id;
        console.log(article_id);
        wx.showModal({
            title: "文章未解锁",
            content: "是否要花费50积分解锁改文章",
            confirmText: "去解锁",
            cancelColor: "#ff0000",
            success: function(temp){
                if(temp.confirm){
                    console.log("去解锁");
                    var user_id = wx.getStorageSync("users").id;
                    app.util.request({
                        url:"entry/wxapp/getcredit",
                        cachetime: "0",
                        data:{
                            user_id: user_id,
                        },
                        success: function(resultNumber){
                            var currentData = this;
                            console.log(currentData);
                            console.log(resultNumber);
                            if(resultNumber.data.money < 50){
                                wx.showModal({
                                    title: "积分不足",
                                    content: "积分不足50无法解锁",
                                    confirmText: "去充值",
                                    cancelColor: "#ff0000",
                                    success: function(temp){
                                        if(temp.confirm){
                                            wx.reLaunch({
                                                url: "../logs/logs"
                                            })
                                        }else if(temp.cancel){
                                            console.log("暂不充值");
                                        }
                                    }
                                })
                            }else{
                                app.util.request({
                                    url: "entry/wxapp/creditdeduct",
                                    cachetime: "0",
                                    data: {
                                        user_id: user_id,
                                        bounty: 50,
                                    },
                                    success: function (temp) {
                                    console.log(temp);
                                    app.util.request({
                                        url:"entry/wxapp/unlockcurrentarticle",
                                        cachetime: "0",
                                        data:{
                                            article_id: article_id,
                                            user_id: user_id,
                                        },
                                        success:function(temp){
                                            wx.showModal({
                                                title:"提示",
                                                content:"解锁成功",
                                                success: function(temp){
                                                    wx.navigateTo({
                                                        url: "../infodetial/infodetial?id=" + article_id,
                                                        success: function(t) {},
                                                        fail: function(t) {},
                                                        complete: function(t) {}
                                                    });
                                                }
                                            })
                                            
                                        }
                                    })
                                    }
                                });

                                
                            }
                        }
                    })
                }else if(temp.cancel){
                    console.log("暂不解锁");
                }
            }
            
        })
    },
    onShow: function() {
        var currentData = this;
        currentData.refresh();
    },
    onShareAppMessage: function(){
        var user_id = wx.getStorageSync("users").id;
        return{   
        success: function(res){
            app.util.request({
                url: "entry/wxapp/chargecredit",
                cachetime: "0",
                data: {
                    user_id: user_id,
                    credit: 0.1
                },
                success: function (temp) {
                    console.log(temp);
                    var serialDate = new Date();
                    var serialYear = serialDate.getFullYear();
                    var serialMonth = serialDate.getMonth() + 1;
                    if (serialMonth < 10) {
                        serialMonth = 0 + "" + serialMonth;
                    }
                    var serialDay = serialDate.getDate();
                    if (serialDay < 10) {
                        serialDay = 0 + "" + serialDay;
                    }
                    var serialHour = serialDate.getHours();
                    if (serialHour < 10) {
                        serialHour = 0 + "" + serialHour;
                    }
                    var serialMins = serialDate.getMinutes();
                    if (serialMins < 10) {
                        serialMins = 0 + "" + serialMins;
                    }
                    var serialNum = serialYear + "" + serialMonth + "" + serialDay + "" + serialHour + "" + serialMins;
                    var date = serialYear + "-" + serialMonth + "-" + serialDay;
                    var time = serialHour + ":" + serialMins;
                    console.log(serialNum);
                    var user_id = wx.getStorageSync("users").id;
                    wx.hideLoading();
                    app.util.request({
                        url: "entry/wxapp/testshareappmessage",
                        cachetime: "0",
                        data: {
                            user_id: user_id,
                            credit: 0.1,
                            date: date,
                            time: time,
                            serial_num: serialNum,
                        },
                        success: function (recordAdded) {
                            console.log(recordAdded);
                            wx.hideLoading({});
                        }
    
                    })
                    
                },
            });
        },
        
        }
    }
})

