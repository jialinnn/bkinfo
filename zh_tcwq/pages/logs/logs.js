var util = require("../../utils/util.js"), app = getApp();

Page({
    data: {
        Return: !1,
    },
    onLoad: function(n) {
        wx.login({
            success: function(tempLogin){
            console.log('Weboost-JL: tempLogin', tempLogin);
            wx.getSetting({
                success: function(tempGetSetting){
                console.log('Weboost-JL: tempGetSetting', tempGetSetting);
                    if(tempGetSetting.authSetting["scope.userInfo"] != true){
                        wx.navigateTo({
                            url: "../authorization/authorization"
                        })
                    }
                }
            })
            }
        })
        var t = this, e = getCurrentPages();
        var currentUser = wx.getStorageSync("users");
        console.log(currentUser);
        e.route = "zh_tcwq/pages/logs/index", 1 == t.data.Return && e.setData({
            Return: !0
        }), wx.setNavigationBarColor({
            frontColor: "#ffffff",
            backgroundColor: wx.getStorageSync("color"),
            animation: {
                duration: 0,
                timingFunc: "easeIn"
            }
        });
        var o = wx.getStorageSync("System").bq_name, c = wx.getStorageSync("System").bq_logo, a = wx.getStorageSync("user_info");
        console.log(a);
        var i = wx.getStorageSync("store"), u = wx.getStorageSync("url");
        console.log(i), t.setData({
            store: i,
            url: u,
            System: wx.getStorageSync("System"),
            // support: o,
            //bq_logo: c,
            bgColor : wx.getStorageSync("bgColor"),
            history: true,
            hiddenHistory: false,
            articles: false,
            hiddenArticles: true,
            score: false,
            hiddenScore: true,
            currentUser: currentUser,
        }), t.setData({
            avatarUrl: a.avatarUrl,
            nickName: a.nickName
        }), app.util.request({
            url: "entry/wxapp/Signset",
            cachetime: "0",
            success: function(n) {
                console.log("签到设置", n), t.setData({
                    qdset: n.data
                });
            }
        }),
        app.util.request({
            url:"entry/wxapp/getpostnumber",
            cachetime: "0",
            data:{
                user_id: currentUser.id,
            },
            success: function(resultNumber){
                var currentData = this;
                console.log(currentData);
                console.log(resultNumber);
              if (resultNumber.data.num == ""){
                t.setData({
                    postArticleNumber: 0,
                })
              }else{
                t.setData({
                  postArticleNumber: resultNumber.data.num,
                })
              }
                
            }
        });
        app.util.request({
            url:"entry/wxapp/getanswernumber",
            cachetime: "0",
            data:{
                user_id: currentUser.id,
            },
            success: function(resultNumber){
                var currentData = this;
                console.log(currentData);
                console.log(resultNumber);
              if (resultNumber.data.num == ""){
                t.setData({
                  answerArticleNumber: 0,
                })
              }else{
                t.setData({
                  answerArticleNumber: resultNumber.data.num,
                })
              }
                
            }
        });
        app.util.request({
            url:"entry/wxapp/getcredit",
            cachetime: "0",
            data:{
                user_id: currentUser.id,
            },
            success: function(resultNumber){
                var currentData = this;
                console.log(currentData);
                console.log(resultNumber);
              if (resultNumber.data.money == null){
                t.setData({
                  userCredit: 0,
                })
              }else{
                t.setData({
                  userCredit: resultNumber.data.money,
                })
              }
                
            }
        });
        wx.hideLoading({});
    },

    selectedL: function(temp){
        var currentData = this;
        currentData.setData({
            history: true,
            hiddenHistory: false,
            articles: false,
            hiddenArticles: true,
            score: false,
            hiddenScore: true,
        })
    },
    selectedM: function(temp){
        var currentData = this;
        currentData.setData({
            history: false,
            hiddenHistory: true,
            articles: true,
            hiddenArticles: false,
            score: false,
            hiddenScore: true,
        })
    },
    selectedR: function(temp){
        var currentData = this;
        currentData.setData({
            history: false,
            hiddenHistory: true,
            articles: false,
            hiddenArticles: true,
            score: true,
            hiddenScore: false,
        })
    },
    collection: function(n) {
        wx.navigateTo({
            url: "../Collection/Collection"
        });
    },
    settled: function(n) {
        wx.navigateTo({
            url: "../settled/settled"
        });
    },
    yellow_page: function(n) {
        wx.navigateTo({
            url: "../yellow_page/mine_yellow",
            success: function(n) {},
            fail: function(n) {},
            complete: function(n) {}
        });
    },
    jfsc: function() {
        wx.navigateTo({
            url: "../integral/integral"
        });
    },
    wdbm: function() {
        wx.navigateTo({
            url: "../wdbm/wdbm"
        });
    },
    tchhr: function(n) {
        var t = wx.getStorageSync("users").id;
        app.util.request({
            url: "entry/wxapp/MyDistribution",
            cachetime: "0",
            data: {
                user_id: t
            },
            success: function(n) {
                console.log(n.data), "2" == n.data.state ? (console.log("是分销商"), wx.navigateTo({
                    url: "../distribution/yaoqing"
                })) : "1" == n.data.state ? wx.showModal({
                    title: "提示",
                    content: "您的申请正在审核中，请耐心等待"
                }) : wx.navigateTo({
                    url: "../distribution/jrhhr"
                });
            }
        });
    },
    my_post: function(n) {
        wx.navigateTo({
            url: "../mypost/mypost",
            success: function(n) {},
            fail: function(n) {},
            complete: function(n) {}
        });
    },
    myAppointment: function(){
        var user=wx.getStorageSync("users");
        wx.navigateTo({
            url:"../skype/appointment?id=" + user.id,
        })
    },
    proAppointment: function(){
        var user=wx.getStorageSync("users");
        wx.navigateTo({
            url:"../skype/proAppointment?id=" + user.id,
        })
    },
    addCredit: function(){
        wx.navigateTo({
            url:"../chargeCredit/chargeCredit",
            success: function(n) {},
            fail: function(n) {},
            complete: function(n) {}
        })
    },
    applyWithdraw: function () {
        wx.navigateTo({url:"../applyWithdraw/applyWithdraw",});
    },
    withdrawRecord: function () {
        wx.navigateTo({url:"../withdrawRecord/withdrawRecord",});
    },
    content: function(n) {
        wx.navigateTo({
            url: "../content/content",
            success: function(n) {},
            fail: function(n) {},
            complete: function(n) {}
        });
    },
    logs_store: function(n) {
        wx.navigateTo({
            url: "bbaa",
            success: function(n) {},
            fail: function(n) {},
            complete: function(n) {}
        });
    },
    order: function(n) {
        wx.navigateTo({
            url: "order",
            success: function(n) {},
            fail: function(n) {},
            complete: function(n) {}
        });
    },
    payment: function(n) {
        wx.navigateTo({
            url: "order?activeIndex=0",
            success: function(n) {},
            fail: function(n) {},
            complete: function(n) {}
        });
    },
    payment_one: function(n) {
        wx.navigateTo({
            url: "order?activeIndex=1",
            success: function(n) {},
            fail: function(n) {},
            complete: function(n) {}
        });
    },
    payment_two: function(n) {
        wx.navigateTo({
            url: "order?activeIndex=2",
            success: function(n) {},
            fail: function(n) {},
            complete: function(n) {}
        });
    },
    payment_three: function(n) {
        wx.navigateTo({
            url: "order?activeIndex=3",
            success: function(n) {},
            fail: function(n) {},
            complete: function(n) {}
        });
    },
    payment_four: function(n) {
        wx.navigateTo({
            url: "order?activeIndex=4",
            success: function(n) {},
            fail: function(n) {},
            complete: function(n) {}
        });
    },
    help: function(n) {
        wx.navigateTo({
            url: "../store/help",
            success: function(n) {},
            fail: function(n) {},
            complete: function(n) {}
        });
    },
    wallet: function(n) {
        wx.navigateTo({
            url: "income",
            success: function(n) {},
            fail: function(n) {},
            complete: function(n) {}
        });
    },
    mine_car: function(n) {
        wx.navigateTo({
            url: "mine_car",
            success: function(n) {},
            fail: function(n) {},
            complete: function(n) {}
        });
    },
    address: function(n) {
        var t = wx.getStorageSync("users").id;
        wx.chooseAddress({
            success: function(n) {
                console.log(n), app.util.request({
                    url: "entry/wxapp/UpdAdd",
                    cachetime: "0",
                    data: {
                        user_id: t,
                        user_name: n.userName,
                        user_tel: n.telNumber,
                        user_address: n.provinceName + n.cityName + n.countyName + n.detailInfo
                    },
                    success: function(n) {
                        console.log(n);
                    }
                });
            }
        });
    },
    jump: function(n) {
        wx.navigateToMiniProgram({
            appId: wx.getStorageSync("System").tz_appid,
            path: "",
            extraData: {
                foo: "bar"
            },
            envVersion: "develop",
            success: function(n) {
                console.log("跳转成功"), console.log(n);
            }
        });
    },
    about: function(n) {
        wx.navigateTo({
            url: "system",
            success: function(n) {},
            fail: function(n) {},
            complete: function(n) {}
        });
    },
    view_unlock_history: function(temp){
        wx.navigateTo({
            url:"../viewHistory/viewHistory",
            success: function(temp) {},
            fail: function(temp) {},
            complete: function(temp) {}
        })
    },
    onReady: function() {},
    onShow: function() {
        this.onLoad();
    },
    onHide: function() {},
    onUnload: function() {},
    onPullDownRefresh: function() {},
    onReachBottom: function() {},
    // onShareAppMessage: function() {
    //     var user_id = wx.getStorageSync("users").id;
    //     return{   
    //     success: function(res){
    //         app.util.request({
    //             url: "entry/wxapp/chargecredit",
    //             cachetime: "0",
    //             data: {
    //                 user_id: user_id,
    //                 credit: 0.1
    //             },
    //             success: function (temp) {
    //                 console.log(temp);
    //                 var serialDate = new Date();
    //                 var serialYear = serialDate.getFullYear();
    //                 var serialMonth = serialDate.getMonth() + 1;
    //                 if (serialMonth < 10) {
    //                     serialMonth = 0 + "" + serialMonth;
    //                 }
    //                 var serialDay = serialDate.getDate();
    //                 if (serialDay < 10) {
    //                     serialDay = 0 + "" + serialDay;
    //                 }
    //                 var serialHour = serialDate.getHours();
    //                 if (serialHour < 10) {
    //                     serialHour = 0 + "" + serialHour;
    //                 }
    //                 var serialMins = serialDate.getMinutes();
    //                 if (serialMins < 10) {
    //                     serialMins = 0 + "" + serialMins;
    //                 }
    //                 var serialNum = serialYear + "" + serialMonth + "" + serialDay + "" + serialHour + "" + serialMins;
    //                 var date = serialYear + "-" + serialMonth + "-" + serialDay;
    //                 var time = serialHour + ":" + serialMins;
    //                 console.log(serialNum);
    //                 var user_id = wx.getStorageSync("users").id;
    //                 wx.hideLoading();
    //                 app.util.request({
    //                     url: "entry/wxapp/testshareappmessage",
    //                     cachetime: "0",
    //                     data: {
    //                         user_id: user_id,
    //                         credit: 0.1,
    //                         date: date,
    //                         time: time,
    //                         serial_num: serialNum,
    //                     },
    //                     success: function (recordAdded) {
    //                         console.log(recordAdded);
    //                         wx.hideLoading({});
    //                     }
    
    //                 })
                    
    //             },
    //         });
    //     },
        
    //     }
    // }
});