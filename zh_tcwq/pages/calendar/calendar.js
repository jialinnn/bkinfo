Page({
  data: {
    date: "",
    dateTomorrow: "",
    chooseDate: "",
    flag: false,
    time:""
  },
  onLoad: function (temp) {
    var currentData = this;
    var todayDate = new Date();
    var tomorrowDate = new Date(todayDate.getTime() + (24 * 60 * 60 * 1000));
    var year = todayDate.getFullYear();
    var month = todayDate.getMonth() + 1;
    var day = todayDate.getDate();
    var hour = todayDate.getHours();
    var mins = todayDate.getMinutes();
    var yearTomorrow = tomorrowDate.getFullYear();
    var monthTomorrow = tomorrowDate.getMonth() + 1;
    var dayTomorrow = tomorrowDate.getDate();
    if (mins < 10) {
      mins = "0" + mins;
    }
    console.log(year + "-" + month + "-" + day + "  " + hour + ":" + mins + "  " + yearTomorrow + "-" + monthTomorrow + "-" + dayTomorrow);
    this.date = year + "-" + month + "-" + day;
    this.dateTomorrow = yearTomorrow + "-" + monthTomorrow + "-" + dayTomorrow;
    this.chooseDate = this.dateTomorrow;
    this.time = hour + ":" + mins;
    console.log(this.date + "  " + this.dateTomorrow);
    currentData.setData({
      date: this.date,
      dateTomorrow: this.dateTomorrow,
      time: hour + ":" + mins,
      chooseDate: this.dateTomorrow,
    })
  },
  cancelAppointment: function(temp){
    wx.switchTab({
      url:"../index/index",
    })
  },
  bindDateChange: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.chooseDate = e.detail.value;
    this.setData({
      dateTomorrow: this.chooseDate,
    })
  },
  bindTimeChange: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      time: e.detail.value
    })
  },
  datePicked: function (temp) {
    var currentData = this;
    var tomorrowDateIs = this.dateTomorrow;
    console.log(this.dateTomorrow + " " + this.chooseDate);
    var dateFlag = this.dateTomorrow.split("-");
    var dateChoose = this.chooseDate.split("-");
    console.log(currentData);
    console.log(temp);
    var start = dateFlag[0] + "/" + dateFlag[1] + "/" + dateFlag[2];
    var end = dateChoose[0] + "/" + dateChoose[1] + "/" + dateChoose[2];
    console.log(start + " " + end);
    var startDateTemp = new Date(start);
    var endDateTemp = new Date(end);
    if (startDateTemp > endDateTemp) {
      console.log(startDateTemp + ">" + endDateTemp);
      this.flag = false;
    } else if (startDateTemp < endDateTemp) {
      console.log(startDateTemp + "<" + endDateTemp);
      this.flag = true;
    } else {
      console.log(startDateTemp + "=" + endDateTemp);
      this.flag = true;
    }
    console.log(this.flag);
    console.log(this.chooseDate);
    console.log(this.time);
    if(this.flag==true){
      wx.navigateTo({
        url:"../skype/skype?date="+this.chooseDate+"&time="+ this.time
      })
    }
    else{
      wx.showModal({
        title: "提示",
        content: "抱歉，您必须提前至少一天预约",
        showCancel: !0,
        cancelText: "取消",
        confirmText: "确定",
        success: function (t) {
          wx.reLaunch({
            url: "calendar",
          })
        },
        fail: function (t) { },
        complete: function (t) { }
      });
    }
  }
})